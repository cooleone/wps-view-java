package com.web.wps.config;

import java.util.HashMap;
import java.util.Map;

public class Context {

    // wps bi传递的参数，统一放在上下文中，方便使用

    private static final ThreadLocal<Map<String, String>> securityMap = new ThreadLocal<>();

    private Context() {
    }

    private static void init() {
        if (securityMap.get() == null) {
            Map<String, String> map = new HashMap<>();
            securityMap.set(map);
        } else {
            securityMap.get().clear();
        }
    }

    static void setToken(String tokenKey) {
        init();
        securityMap.get().put(ReqKeys.TOKEN_KEY, tokenKey);
    }

    public static String getToken() {
        if (securityMap.get() == null)
            return null;
        else
            return securityMap.get().get(ReqKeys.TOKEN_KEY);
    }

    static void setFileId(String fileId) {
        securityMap.get().put(ReqKeys.FILE_ID_KEY, fileId);
    }

    public static String getFileId() {
        if (securityMap.get() == null)
            return null;
        else
            return securityMap.get().get(ReqKeys.FILE_ID_KEY);
    }

    static void setAgent(String agent) {
        securityMap.get().put(ReqKeys.USER_AGENT_KEY, agent);
    }

    public static String getAgent() {
        if (securityMap.get() == null)
            return null;
        else
            return securityMap.get().get(ReqKeys.USER_AGENT_KEY);
    }

    static void setAppId(String appId) {
        securityMap.get().put(ReqKeys.APP_ID_KEY, appId);
    }

    public static String getAppId() {
        if (securityMap.get() == null)
            return null;
        else
            return securityMap.get().get(ReqKeys.APP_ID_KEY);
    }

    static void setSignature(String signature) {
        securityMap.get().put(ReqKeys.SIGNATURE_KEY, signature);
    }

    public static String getSignature() {
        if (securityMap.get() == null)
            return null;
        else
            return securityMap.get().get(ReqKeys.SIGNATURE_KEY);
    }

    static void setUserId(String userId) {
        securityMap.get().put(ReqKeys.USER_ID_KEY, userId);
    }

    public static String getUserId() {
        if (securityMap.get() == null)
            return null;
        else
            return securityMap.get().get(ReqKeys.USER_ID_KEY);
    }

}
